'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  BASE_API: '"http://192.168.109.132:7300/mock/605bdf614bf8ad0e7116f0d7/tensquare"',
})
